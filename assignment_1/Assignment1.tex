\documentclass[11pt, a4paper]{article}
\usepackage[utf8]{inputenc}

\usepackage{geometry}
\geometry{
    a4paper,
    bottom=30mm
}

\setlength{\parskip}{\baselineskip}
\setlength{\parindent}{0pt}

\usepackage{makecell}

\usepackage{listings}

\usepackage{graphicx}

\usepackage{pdflscape}

\usepackage{hyperref}

\title{
    CMP2804M Team Software Engineering\\
    Assignment 1, Interim Report}
\author{Group 33: Pawel Bielinski (18679372), Christopher Cogdon (18687297),\\
Jamie Godwin (18675053), Callum Lancaster (14531914), Shiqi Ma (18689183),\\
Elliot Miller (17666071), Matthew Murr (17664330)}

\usepackage{fancyhdr}
\pagestyle{fancy}
\lhead{CMP2804M Team Software Engineering Interim Report (Assessment 1)}
\rhead{Group 33}
\lfoot{Pawel Bielinski (18679372), Christopher Cogdon (18687297),
Jamie Godwin (18675053), Callum Lancaster (14531914), Shiqi Ma (18689183),
Elliot Miller (17666071), Matthew Murr (17664330)}
\cfoot{}
\rfoot{\thepage}
\renewcommand{\headrulewidth}{0pt}

\begin{document}

\maketitle

\pagebreak

\tableofcontents

\pagebreak

\section{Introduction}

C++ has been an influential language for many decades. However, many users
have noted that it holds on to features that modern languages have replaced or
removed. We propose a
C++-based alternative language called CNatural that removes redundant syntax
features and present a plan to implement related language tools including a
compiler and syntax highlighter.

\section{Project Proposal}

\subsection{Goal}

The goal of the project is to release a Free and Open Source Software (FOSS)
implementation of a C++-based language with reduced syntax redundancy.

\subsection{Implementation}

\subsubsection{Compiler}

The core of the project will be the compiler. Towards the
goal of removing redundancy, the following specific feature replacements have
been identified as key:
\begin{itemize}
    \item Curly-braces with indentation for code blocks (example in Appendix A),
        using the first indent to allow variable indentation styles
    \item Using newlines over semicolons to end statements, by inserting
        semicolons after statements allowing it
    \item The removal of the requirement for header files, by parsing for
        definitions and spliting into definition (header) and implementation
        (cpp file)
\end{itemize}

CNatural will not add new structures to the language, only remove unnecessary
syntax. (Aho et al., 2007, 4) describes the two major
pieces of compilation, analysis and synthesis (front-end and back-end).
The CNatural compiler will only consider the changed syntax features of the
language and translate them to valid C++ code,
essentially creating an intermediate representation of the source
(Aho et al., 2007, 9) to be handled by a C++ compiler such as GCC. Hence, the
CNatural compiler will be almost entirely front-end.

In order to implement this, rather than implement a hand-crafted compiler, we
will use so-called compiler-compiler tools. We will use Flex and Bison
(FOSS implementations of lex and yacc) (Levine, 2009), which are parser builders
that simplifiy the process of compiler creation, and are used in GCC for
some parts of C and C++ parser building (GNU, 2019). Appendix A contains a
simple example that demonstrates the replacement of curly-brace blocks with
indentation blocks.

\subsubsection{Development Tools}

To improve the usability of CNatural for programmers, integration will be
provided with gcc and gdb, as well as build tools such as make and CMake.
We will also create a syntax highlighter compatible with vim.

\subsection{Plan}

Development will follow an Agile philosophy. The focus on
"Responding to change over following a plan" (Beck K. et al, 2001) suits our
project's variable milestones, and allows the team to fill roles as needed and
react to potentially unknown development requirements as we uncover them.

\subsubsection{Roles}

There is an emphasis in Agile on
"Individuals and interactions", so it followed that it would be
effective to split the work into several interacting specialisations. From
research into compilers (Aho et al., 2007), Flex/Bison (Levine, 2009), C++ syntax and grammar (ISO, 2017),
build tools and syntax highlighting, the following roles were decided upon:

\begin{itemize}
    \item Documentation -- Working with the entire team to ensure common
        understanding and access to knowledge where required
    \item Compiler Maintainer -- Focuses on developing
        compiler features
    \item C++ Specialist -- Ensures compiler and tools work across C++'s
        complex grammar and feature set
    \item Build System Specialist -- Ensures target build tools are easy to use
        alongside or through the CNatural compiler
    \item Syntax Highlighting -- Maintains a syntax highlighter for CNatural
        in vim
    \item "Fast" Compiler Developer -- Researches and implements new features
        ahead of schedule to allow them to be brought into the main compiler
\end{itemize}

\subsubsection{PERT \& Gantt Chart}

A PERT chart was created to visualise the tasks, milestones and their
dependencies over the course of the project. A Gantt chart is also provided as a snapshot of what we
think the general flow of the project will entail.

\pagebreak
\begin{landscape}
\clearpage
\thispagestyle{empty}
    \begin{figure}[b]
        \includegraphics[scale=0.3]{pert}
    \end{figure}
\end{landscape}

\pagebreak
\begin{landscape}
\clearpage
\thispagestyle{empty}
    \begin{figure}[b]
        \includegraphics[scale=2.2]{gantt}
    \end{figure}
\end{landscape}

\restoregeometry

\subsubsection{Risk Assessment}
As a tool for analysing and dealing with risk, the US Government has published
(Software Development Risk Assessment, n.d.), which has been applied to the
project:

Low – very unlikely to happen

Medium – Roughly 50 – 50 chance to happen

High – very likely to happen

Scope planning, Medium – Initially failing to identify certain requirements,
can cause setbacks if it interferes with critical deliverables in the project
plan. To minimise this, extensive research was performed and parallel goals
identified and designed as roles. Additionally, fortnightly Agile meetings will
be held to identify such problems as they arise.

People management, High – Assigned roles may require more members or
additional training, not managing members can cause delays in production with
falling behind schedule. We will use the first month to ensure members are
familiar with their goals and the tools they will use, and can adapt role
assignments as necessary.

Unrealistic Schedules, Low – Estimating schedules at the beginning of the
project are unlikely to align with implementing the project. Mitigation will
involve measuring progress with Agile sprints and regular updating of plans
during fortnightly meetins. To help gauge the project timeline, the Gantt chart
shows a rough snapshot.

\pagebreak

\section{Domain Research}

\subsection{Justification}

CNatural has the potential to be the next step in the evolution of
user-friendly programming languages. Its intent is to take away the issues a
lot of programmers have had with headers and syntax in C++ (Stack Overflow,
2016) "Is there any way to not have to write function declarations twice
(headers) and still retain the same scalability in compiling, clarity in
debugging, and flexibility in design when programming in C++?". Removing
parts and simplifying the language will allow for a great scope of people to
start programming as it will more accessible. Following in the foot step of
C++, that was originally compiled C.

\subsection{SWOT Analysis}

Strengths -- The Strength of CNatural is that it is a more simple language,
which means it can be pick up and used by new users more easily, while still
having the strength of C++.

Weaknesses -- CNatural will add another step to the compilation process,
not only meaning that CN files will take longer to compile than C++ code, but
also potentially requiring programmers to learn and interact with another tool.
This means it could be seen as more of a hinderance than a help.

Opportunities -- There are 2 main opportunities that CNatural gains by being
simpler and open source. As the Language is open source it can be worked on by
many different people, improving the language in ways a solo team cannot
(Raymond, 2001). Also with it removing unnecessary features a good gateway language which allows more people to get into programming easier.

Threats -- The threat to C Natural is that it is intended to be open source.
This allows many different people to  have  access to  the  source code  which
runs  the  risk  of  malicious  actors  posting  code  which could cause harm.
To minimize this risk the team would have people verifying code to ensure that
it is not harmful before including it in the project.

\pagebreak

\section{Legal, Ethical and Political Statement}

\subsection{Ethical}

CNatural will be developed as Free and Open Source software. This will allow us
to distribute the source code to the community which fits in the belief of
Utilitarianism (Wiley Online Library 2016): "The Needs of the Many Outweigh the
Needs of the Few" - Greg Littmann. In this situation the greater programing
community would be the many who require a revised and improved programming
language, the software team would be the few as we sacrifice control over the
project so that it can have all the benefits of Free software. Free software
allows creators to use the software freely and be able to view, modify and
distribute the software's source code. It allows individuals to work and
improve projects without infringing on copyright or IP and also makes it widely
accessible, allowing large groups to build projects together. By having
CNatural as free software it can also be used as an educational resource:
(Edutopia, 2011) "free software is a gateway for students to explore and learn"
- Shahzad Saeed. All students will have access to use and develop CNatural due
to it being Free software. It will allow students to learn and develop
programing skills and techniques, and even improve CNatrual itself by committing new features and functions to the project.

\subsection{Legal}

CNatural won't collect any user data and will therefore be completely in line
with the 2018 Data Protection Act as we will not have any personal data stored.
When working on the project we must make sure to follow the Computer Misuse
Act. There are 3 main points to the Computer Misuse Act that we must pay close attention to and abide by. The first is (Legislation.gov) “Unauthorized access to computer material.” This is when you attempt to gain access to any program or data held in any computer without authorization. (Legislation.gov ) “Unauthorized access to a computer with intent to commit or facilitate the commission or a further offence” This is very smiler to the first rule however it is about committing a further offence once they have accused to the system. (Legislation.gov ) “Unauthorized modification of computer material” This is when someone has access to data and will edit it.While creating CNatural all members of the group must make sure that all work committed to the project is our own and that none of the work has been stolen or used without consent. We will make sure to follow this be keeping track of copyrights for any work which we wish to use and will reference all data we use. (gov.uk/copyright) “Copyright protects your work and stops others from
using it without your permission” as our software will be a free software, we don’t need to worry about the CNatural copyright.As this project is starting out as a small Development team, we don’t have a set of expectations of where stockholders would like the project to be and how to distribute the software. This allows the Team to develop CNatural the way our group would like it to be. This helps to keep the project simple and we won't be held accountable by stakeholders demands about the project. This also gives our group full control of projected allowing us to distribute it however we wish keeping the legal side of the project very simple.

\subsection{Political}

Under the terms of our licence, we have no responsibility for and are not in control of anything that our users make using the CNaturalcompiler or related tools, andwill not be held accountable for any damage caused. We also do not have any ownership over any programs created by users of the software.

\pagebreak

\section{Group Reflection}

\subsection{Account}

Initially, we scheduled weekly meetings, alongside fortnightly meetings with
the supervisor (Charles).
Before meeting Charles, we developed and researched ideas, and the compiler
technologies that would allow us to achieve them.
It was decided that Matt would be the point of contact for Charles. Messages
to Charles were sent out for guidance on the
project, due to the group having limited experience with the topic. Trello and
Microsoft Teams was set up by Elliot and Jamie to manage ideas and share
resources, as well as a task list. These felt appropriate for the first part of
the task as there is no coding involved and roles can be assigned for the write
up. We developed a list of ideas for Charles to review. However, we ran into
scheduling problems, therefore this was delayed by a week. This was a minor
setback as the group was unable to work without his feedback having little
experience in the topic. Having Charles’s input on our ideas we knew that we
had a wide choice of projects, as we’ve gotten positive feedback on each one.
However, the CNatural project was best received, which allows flexibility in
case of failure. Using a decision matrix (see appendix) the group came to the
decision that this was the optimal idea. Moving forwards the group will focus
on learning the required tools for their role. Preliminary role assignment has
been done based on what the group members felt they were the most competent in
/ liked the most, each member also filled out the Belbin chart to help with
this.

\subsection{Roles and Attendance}

\begin{tabular}{|c|c|c|c|c|}
\hline
\thead{{\bf Name}} & \thead{{\bf Student ID}} & \thead{{\bf Belbin Role}} &
\thead{{\bf Project Role}} & \thead{{\bf Meeting} \\ {\bf Attendance (\%)}}\\
\hline
Pawel Bielinski & 18679372 & Implementer & C++ Specialist & 95\\
\hline
Christopher Cogdon & 18687297 & Team Worker & \makecell{Preliminary \\ Compiler}
& 100\\
\hline
Jamie Godwin & 18675053 & Plant & \makecell{Vim Syntax \\ Highlighting} & 95\\
\hline
Callum Lancaster & 14531914 & Shaper & Documentation & 100\\
\hline
Shiqi Ma & 18689183 & \makecell{Build System \\ Integration} & Implementer & 25\\
\hline
Elliot Miller & 17666071 & Completer Finisher & \makecell{Build System \\
Integration} & 95\\
\hline
Matthew Murr & 17664330 & Co-Ordinator & \makecell{Flex \& Bison \\
Specialist} & 100\\
\hline

\end{tabular}

\pagebreak

\section{Conclusion}

The concept for a new programming language based on C++ (named CNatural) has
been introduced, and a plan for implementation over the span of several months
has been proposed. The ramifications of its development and
release as Free and Open Source software has been considered, as well as the
implications of its position as a compiler. The development of compilimentary
software and documentation, such as a syntax highlighter, has also been included.
Finally, the team's ability to work together has been reviewed and methods for
continuing to work effectively and to improve suggested.

\pagebreak

\section{References}

{\it Software Development Risk Assessment}, (n.d.) Washington D.C, USA: Office
of the Chief Information Officer, U.S. Department of Energy.

Aho A., Lam M., Sethi R., and Ullman J. (2007) {\it Compilers: Principles,
Techniques and Tools}, 2nd Edition. Boston, USA: Addison-Wesley\\

Levine J. (2009) {\it Flex \& Bison}, 1st Edition. Sebastopol, USA: O’Reilly\\

ISO and IEC (2017) {\it C++ International Standard}. ISO/IEC 14882:2017.
Geneva, Switzerland: ISO. Available from
https://www.iso.org/standard/68564.html [accessed 10 November 2019].

GNU (2019) {\it GCC Compiler} [software]. GNU. Available from
https://github.com/gcc-mirror/gcc/ [accessed 12 November 2019]\\

Beck K., Beedle M., van Bennekum, A., Cockburn A., Cunningham W., Fowler M.,
Grenning J., Highsmith J., Hunt A., Jeffries R., Kern J., Marick B., Martin R.,
Mellor S., Schwaber K., Sutherland J., Thomas D. (2001) {\it The Agile
Manifesto}. Available from https://agilemanifesto.org/ [accessed 12 November
2019]\\

Stack Overflow (2016) {\it Can I write C++ code without headers?} [forum]. Available from
https://stackoverflow.com/questions/1001639/can-i-write-c-code-without-headers-repetitive-function-declarations [accessed 4 November 2019]\\

Littmann, G. (2016). Utilitarianism and Star Trek. [online] Wiley online
libary. Available at:
https://onlinelibrary.wiley.com/doi/abs/10.1002/9781119146032.ch12[Accessed 11
Nov. 2019].\\

Saeed, S. (2011). Top 10 Benefits of Using Free Software. [online] Edutopia.
Available at:
https://www.edutopia.org/blog/benefits-free-software-shahzad-saeed[Accessed 11
Nov. 2019].\\

Legislation.gov (1990). Computer Misuse Act 1990. [online] Legislation.gov.uk.
Available at: https://www.legislation.gov.uk/ukpga/1990/18/section/1[Accessed
12 Nov. 2019].\\

GOV.UK. (2017). How copyright protects your work. [online] Available at:
https://www.gov.uk/copyright[Accessed 12 Nov. 2019].\\

\pagebreak

\section{Appendix A -- Example Parser}

\subsection{cn.l}
An example Flex rule file. When the rules between the \%\%s are matched for a
given line, the corresponding token as defined in the Bison file are
returned.\\

\begin{lstlisting}[language=c++]
%{

#include <stdio.h>
#include <string.h>

#include "cn.tab.h"

%}

%%

^[\t]           { // For simplicity, only tabs are currently supported.
                    return START_INDENT;
                }
[\t]            {
                    return INDENT;
                }
\n              {
                    return NL;
                }
[A-Za-z#\/].+   { // This regex is a temporary simplification.
                    yylval.sval = strdup(yytext); // Will be free()d by bison.
                    return STATEMENT;
                }

%%
\end{lstlisting}

\subsection{cn.y}
An example Bison rule file. It defines the syntax and corresponding logic that
the file will be parsed with.\\

\begin{lstlisting}[language=c++]
%{

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_PATH 4096

// Flex Definitions
extern int yylex();
extern int yyparse();
extern FILE *yyin;

void yyerror(const char *s);

// Required Globals
FILE *fout;

int indentPrev = 0;

// Function Definitions
void addBrackets();
int parse(char *finpath);

%}

%union {
    int ival;
    char *sval;
}

%token NL
%token START_INDENT
%token INDENT
%token <sval> STATEMENT

%type <ival> line_indent
%type <sval> codeline

%start codefile

%%

codefile:
        | codefile line         { }
;

line:
        NL                      { fprintf(fout, "\n"); }
        | codeline NL           { fprintf(fout, "%s\n", $1); free($1); }
;

codeline:
        STATEMENT               { addBrackets(0); $$ = $1; }
        | line_indent STATEMENT { addBrackets($1); $$ = $2; }

line_indent:
        START_INDENT            { $$ = 1; }
        | line_indent INDENT    { $$ = $1 + 1; }
;

%%

void addBrackets(int indentCurr)
{
    int indentDiff = indentCurr - indentPrev;

    if (indentDiff > 0)
    {
        for (int i = 0; i < indentDiff; ++i)
        {
            fprintf(fout, "{");
        }
    }
    else if (indentDiff < 0)
    {
        for (int i = 0; i < -indentDiff; ++i)
        {
            fprintf(fout, "};");
        }
    }

    // Print tabs just to make output look prettier.
    for (int i = 0; i < indentCurr; ++i)
    {
        fprintf(fout, "\t");
    }

    indentPrev = indentCurr;
}

int parse(char filepath[MAX_PATH])
{

    FILE *fin = fopen(filepath, "r");
    if (!fin)
    {
        return 1;
    }
    yyin = fin;

    // We make some assumptions about filepath here.
    // It would be better to write a function that handles path conversion in more detail.
    char *pch;
    pch = strstr(filepath, ".cn");
    strncpy(pch, ".cpp\0", 5);
    fout = fopen(filepath, "w+");

    do
    {
        yyparse();
    } while(!feof(yyin));
    addBrackets(0); // Close any open brackets.

    fclose(fin);
    fclose(fout);
}

int main(int argc, char **argv)
{
    if (argc == 1)
    {
        fprintf(stderr, "No files provided.\n");
        exit(1);
    }

    for (int i = 1; i < argc; ++i)
    {
        if (parse(argv[i]))
        {
            fprintf(stderr, "Error writing to file %s\n", argv[i]);
        }
    }
}

void yyerror(const char *s) {
    fprintf(stderr, "Parse error: %s\n", s);
    exit(1);
}
\end{lstlisting}

\subsection{Cat.cn}
An example source file that uses indentations to denote code blocks. The
example compiler will convert this file to valid C++ code.\\

\begin{lstlisting}[language=c++]
#include <iostream>

class Cat
	private:

	int age;
	int legs;

	public:

	Cat(int age, int legs = 4)
		this->age = age;
		this->legs = legs;

	void speak(int x)
		for (int i = 0; i < x; i++)
			std::cout << "meow\n";

	int getAge()
		return this->age;

int main()
	Cat purrcival = Cat(6);
	purrcival.speak(2);
\end{lstlisting}

\subsection{Cat.cpp}
The output of running the example compiler on Cat.cn.\\

\begin{lstlisting}[language=c++]
#include <iostream>

class Cat
{	private:

	int age;
	int legs;

	public:

	Cat(int age, int legs = 4)
{		this->age = age;
		this->legs = legs;

};	void speak(int x)
{		for (int i = 0; i < x; i++)
{			std::cout << "meow\n";

};};	int getAge()
{		return this->age;

};};	int main()
{	Cat purrcival = Cat(6);
	purrcival.speak(2);
};
\end{lstlisting}

\pagebreak

\section{Appendix B -- Tables}
\subsection{Decision Matrix}
\begin{tabular}{|c|c|c|c|c|}
    \hline
    Item & Feasibility & Group Interest & Expandability & Total Score\\
    \hline
    (Corresponding Weights)
        & 5 & 4 & 2 & N/A\\
    \hline
    \makecell{Text Based \\ Adventure with \\ Programming solutions}
        & 3 (15) & 1 (4) & 1 (2) & 21\\
    \hline
    \makecell{Pathfinding Language \\ with Race Game}
        & 1 (5) & 3 (12) & 2 (4) & 21\\
    \hline
    \makecell{Interactive \\ Basic Compiler}
        & 3 (15) & 1 (4) & 2 (4) & 23\\
    \hline
    \makecell{Tower Defence \\ Game with \\ Programming}
        & 2 (10) & 1 (4) & 2 (4) & 18\\
    \hline
    \makecell{C++ with Modern \\ Features/More \\ Natural Syntax}
        & 3 (15) & 2 (8) & 3 (6) & 29\\
    \hline


\end{tabular}

\pagebreak

\section{Appendix C -- Group Score Allocation}
\includegraphics[scale=3]{signed_pcs}

\end{document}
